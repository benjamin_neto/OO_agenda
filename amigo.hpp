#ifndef _AMIGO_H_
#define _AMIGO_H_

#include "pessoa.hpp"

class Amigo:public Pessoa{
	private: 
		string apelido;
		string endereco;

	public:
		Amigo();
		Amigo(string apelido, string endereco);
		string getApelido();
		string getEndereco();
		void setApelido(string apelido);
		void setEndereco(string endereco);
};

#endif
