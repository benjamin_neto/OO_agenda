#include "contato.hpp"
#include<string>

Contato::Contato(){
	setCidade("...");
	setProfissao("...");
}

Contato::Contato(string cidade, string profissao){
	this->cidade=cidade;
	this->profissao=profissao;
}

string Contato::getCidade(){
	return cidade;
}

string Contato::getProfissao(){
	return profissao;
}

void Contato::setCidade(string cidade){
	this->cidade=cidade;
}

void Contato::setProfissao(string profissao){
	this->profissao=profissao;
}

