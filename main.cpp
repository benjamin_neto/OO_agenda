#include "pessoa.cpp"
#include "amigo.cpp"
#include "contato.cpp"
#include<iostream>
#include<string>


int main(){
	Amigo umAmigo;
	Contato umContato;

	umAmigo.setNome ("João");
	umAmigo.setEmail ("joao@email.com");
	umAmigo.setTelefone ("555-5555");
	umAmigo.setApelido ("Jão");
	umAmigo.setEndereco ("Setor Central");
	
	umContato.setNome ("Maria");
	umContato.setEmail ("maria@email.com");
	umContato.setTelefone ("555-4444");
	umContato.setCidade ("Gama");
	umContato.setProfissao ("Jornalista");

	cout << "Amigo \n" << endl;

	cout << "Nome: " << umAmigo.getNome() << endl;
	cout << "Email: " << umAmigo.getEmail() << endl;
	cout << "Telefone: " << umAmigo.getTelefone() << endl;
	cout << "Apelido: " << umAmigo.getApelido() << endl;
        cout << "Endereço: " << umAmigo.getEndereco() << endl;

	cout << "\nContato \n" << endl;

	cout << "Nome: " << umContato.getNome() << endl;
	cout << "Email: " << umContato.getEmail() << endl;
	cout << "Telefone: " << umContato.getTelefone() << endl;
	cout << "Cidade: " << umContato.getCidade() << endl;
        cout << "Profissao: " << umContato.getProfissao() << endl;

	return 0;
}
