#include "pessoa.hpp"
#include <string>

using namespace std;

Pessoa::Pessoa(){
	nome="...";
	email="...";
	telefone="xxx";
}

Pessoa::Pessoa(string nome, string email, string telefone) {
        this->nome=nome;
	this->email=email;
	this->telefone=telefone;
}

string  Pessoa::getNome() {
        return nome;
}

void Pessoa::setNome(string nome) {
        this->nome=nome;
}

string  Pessoa::getEmail() {
        return email;
}

void Pessoa::setEmail(string email) {
        this->email=email;
}

string  Pessoa::getTelefone() {
        return telefone;
}

void Pessoa::setTelefone(string telefone) {
        this->telefone=telefone;
}
