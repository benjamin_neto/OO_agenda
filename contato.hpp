#ifndef _CONTATO_H_
#define _CONTATO_H_

#include "pessoa.hpp"

class Contato:public Pessoa{
	private:
		string cidade;
		string profissao;

	public:
		Contato();
		Contato(string cidade, string profissao);
		string getCidade();
		string getProfissao();
		void setCidade(string cidade);
		void setProfissao(string profissao);
}

#endif	
